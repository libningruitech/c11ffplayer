#pragma once
#include <string>
extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
}

class AudioDecoder
{
public:
    AudioDecoder(AVCodecContext *codec_context);
    ~AudioDecoder();

    void operator()(AVFrame* frame, int &finished, AVPacket* packet);

    unsigned sample_rate() const;
    std::string format() const;
    unsigned channels() const;
    unsigned samples() const;
    bool is_planar() const;
    unsigned bytes() const;


private:
    AVCodecContext* codec_context_{};
    //AVFormatContext* format_context_{nullptr};

    int stream_index_{-1};
};


