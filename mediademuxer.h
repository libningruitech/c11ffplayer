#pragma once
#include <string>
extern "C" {
	#include "libavformat/avformat.h"
}

class MediaDemuxer {
public:
    MediaDemuxer(const std::string &file_name);
    ~MediaDemuxer();
	AVCodecContext* video_codec_context();
	int video_stream_index() const;
	AVRational time_base() const;
	bool operator()(AVPacket &packet);

    //audio
    AVCodecContext* audio_codec_context();
    int audio_stream_index() const;
    void change_audio_stream();// index up or down. !=0 for decrease, 0 for increase


private:
	AVFormatContext* format_context_{};
	int video_stream_index_{};

    //audio
    int audio_stream_index_{};
    int audio_stream_counts_{};

};
