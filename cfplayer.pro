TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt


LIBS += -lavformat -lavcodec -lswscale -lswresample -lavutil  `sdl2-config --cflags --libs` -lasound -lsfml-audio -lsfml-system -lSDL2 -std=c++11 -lpthread -lz


SOURCES += main.cpp \
    alsa_audio.cpp \
    audio_decoder.cpp \
    decode_audio.cpp \
    display.cpp \
    ffmpeg.cpp \
    format_converter.cpp \
    media_decoder.cpp \
    mediademuxer.cpp \
    mediaplayer.cpp \
    ring_buffer.cpp \
    sdl_audio.cpp \
    sfml_audio.cpp \
    timer.cpp \
    video_decoder.cpp \
    playsettings.cpp


HEADERS += \
    alsa_audio.h \
    audio_decoder.h \
    audio_factory.h \
    audio.h \
    display.h \
    ffmpeg.h \
    format_converter.h \
    media_decoder.h \
    mediademuxer.h \
    mediaplayer.h \
    queue.h \
    ring_buffer.h \
    sdl_audio.h \
    sfml_audio.h \
    timer.h \
    video_decoder.h \
    playsettings.h

