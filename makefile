CC = g++
CFLAGS = -g -Wall

LDFLAGS = -lavformat -lavcodec -lswscale -lswresample -lavutil  `sdl2-config --cflags --libs` -lasound -lsfml-audio -lsfml-system -lSDL2 -std=c++11 -lpthread -lz

TARGET = c11ffplayer

all: $(TARGET)


c11ffplayer: main.o mediaplayer.o mediademuxer.o audio_decoder.o playsettings.o video_decoder.o   format_converter.o ffmpeg.o display.o timer.o sdl_audio.o sfml_audio.o alsa_audio.o ring_buffer.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o mediaplayer.o audio_decoder.o mediademuxer.o video_decoder.o   format_converter.o ffmpeg.o display.o timer.o sdl_audio.o sfml_audio.o alsa_audio.o ring_buffer.o playsettings.o $(LDFLAGS)



main.o: main.cpp mediaplayer.h
	$(CC) $(CFLAGS) -c main.cpp $(LDFLAGS)


mediaplayer.o:  mediaplayer.cpp mediaplayer.h demuxer.h playsettings.h video_decoder.h format_converter.h display.h timer.h queue.h audio_decoder.h ring_buffer.h audio_factory.h
	$(CC) $(CFLAGS) -c mediaplayer.cpp $(LDFLAGS)

mediademuxer.o: mediademuxer.cpp mediademuxer.h ffmpeg.h
	$(CC) $(CFLAGS) -c mediademuxer.cpp $(LDFLAGS)

audio_decoder.o: audio_decoder.cpp audio_decoder.h ffmpeg.h
	$(CC) $(CFLAGS) -c audio_decoder.cpp $(LDFLAGS)

playsettings.o: playsettings.cpp playsettings.h
	$(CC) $(CFLAGS) -c playsettings.cpp $(LDFLAGS)

# video part
videoplayer: videomain.o player.o demuxer.o video_decoder.o format_converter.o ffmpeg.o display.o timer.o
	$(CC) $(CFLAGS) -o $(TARGET) videomain.o player.o demuxer.o video_decoder.o format_converter.o ffmpeg.o display.o timer.o $(LDFLAGS)

videomain.o: videomain.cpp player.h
	$(CC) $(CFLAGS) -c videomain.cpp $(LDFLAGS)

player.o: player.cpp player.h demuxer.h video_decoder.h format_converter.h display.h timer.h queue.h
	$(CC) $(CFLAGS) -c player.cpp $(LDFLAGS)

demuxer.o: demuxer.cpp demuxer.h ffmpeg.h
	$(CC) $(CFLAGS) -c demuxer.cpp $(LDFLAGS)

video_decoder.o: video_decoder.cpp video_decoder.h ffmpeg.h
	$(CC) $(CFLAGS) -c video_decoder.cpp $(LDFLAGS)

format_converter.o: format_converter.cpp format_converter.h
	$(CC) $(CFLAGS) -c format_converter.cpp $(LDFLAGS)

ffmpeg.o: ffmpeg.cpp ffmpeg.h
	$(CC) $(CFLAGS) -c ffmpeg.cpp $(LDFLAGS)

display.o: display.cpp display.h
	$(CC) $(CFLAGS) -c display.cpp $(LDFLAGS)

timer.o: timer.cpp timer.h
	$(CC) $(CFLAGS) -c timer.cpp $(LDFLAGS)

# audio part
audioplayer: audiomain.o decode_audio.o sdl_audio.o sfml_audio.o alsa_audio.o ring_buffer.o
	$(CC) $(CFLAGS) -o $(TARGET) audiomain.o decode_audio.o alsa_audio.o sdl_audio.o sfml_audio.o ring_buffer.o $(LDFLAGS)


audiomain.o: audiomain.cpp decode_audio.h ring_buffer.h audio_factory.h
	$(CC) $(CFLAGS) -c audiomain.cpp $(LDFLAGS)

decode_audio.o: decode_audio.cpp decode_audio.h ring_buffer.h
	$(CC) $(CFLAGS) -c decode_audio.cpp $(LDFLAGS)

alsa_audio.o: alsa_audio.cpp ring_buffer.h audio.h
	$(CC) $(CFLAGS) -c alsa_audio.cpp $(LDFLAGS)

sdl_audio.o: sdl_audio.cpp ring_buffer.h audio.h
	$(CC) $(CFLAGS) -c sdl_audio.cpp $(LDFLAGS)

sfml_audio.o: sfml_audio.cpp ring_buffer.h audio.h
	$(CC) $(CFLAGS) -c sfml_audio.cpp $(LDFLAGS)

ring_buffer.o: ring_buffer.cpp ring_buffer.h audio.h
	$(CC) $(CFLAGS) -c ring_buffer.cpp $(LDFLAGS)

clean:
	rm -f *.o $(TARGET)

