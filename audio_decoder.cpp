#include "audio_decoder.h"
#include "ffmpeg.h"

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>

extern "C" {
    #include <libavutil/samplefmt.h>
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
}


AudioDecoder::AudioDecoder(AVCodecContext* codec_context):
    codec_context_{codec_context} {

    avcodec_register_all();
    const auto codec_audio =
        avcodec_find_decoder(codec_context_->codec_id);
    if (!codec_audio) {
        throw ffmpeg::Error{"Unsupported audio codec"};
    }
    ffmpeg::check(avcodec_open2(
        codec_context_, codec_audio, nullptr));

}
AudioDecoder::~AudioDecoder(){
    avcodec_close(codec_context_);
}

void AudioDecoder::operator()(AVFrame* frame, int &finished, AVPacket* packet)
{

    while (packet->size) {
        auto size = avcodec_decode_audio4(
            codec_context_, frame, &finished, packet);
        if (size < 0) {
            throw ffmpeg::Error{"Error decoding audio."};
        }

        auto decoded = std::min(size, packet->size);
        packet->data += decoded;
        packet->size -= decoded;
    }

}



unsigned AudioDecoder::sample_rate() const{
    return codec_context_->sample_rate;
}

std::string AudioDecoder::format() const{
    auto packed = av_get_packed_sample_fmt(
        static_cast<AVSampleFormat>(codec_context_->sample_fmt));
    auto format = av_get_sample_fmt_name(packed);
    return format ? format : std::string{};
}
unsigned AudioDecoder::channels() const{
    return codec_context_->channels;
}
unsigned AudioDecoder::samples() const{
    return 8192;
}
bool AudioDecoder::is_planar() const{
    return av_sample_fmt_is_planar(
        static_cast<AVSampleFormat>(codec_context_->sample_fmt));

}
unsigned AudioDecoder::bytes() const{
    return av_get_bytes_per_sample(
        static_cast<AVSampleFormat>(codec_context_->sample_fmt));

}
