#include "playsettings.h"
#include <stdexcept>

template <typename T>
inline T check_SDL_E(T value, const std::string &message) {
    if (!value) {
        throw std::runtime_error{"SDL " + message};
    } else {
        return value;
    }
}

SDL_E::SDL_E() {
    check_SDL_E(!SDL_InitSubSystem(SDL_INIT_EVENTS), "init");
}

SDL_E::~SDL_E() {
    SDL_QuitSubSystem(SDL_INIT_EVENTS);;
}


PlaySettings::PlaySettings()
{
}


void PlaySettings::input() {
    if (SDL_PollEvent(&event_)) {
        switch (event_.type) {
        case SDL_KEYUP:
            switch (event_.key.keysym.sym) {
            case SDLK_ESCAPE:
                quit_ = true;
                break;
            case SDLK_SPACE:
                play_ = !play_;
                break;
            case SDLK_DOWN:
            case SDLK_UP:
                nextTrackFlag = true;
                break;
            default:
                break;
            }
            break;
        case SDL_QUIT:
            quit_ = true;
            break;
        default:
            break;
        }
    }
}

bool PlaySettings::get_quit() {
    return quit_;
}

bool PlaySettings::get_play() {
    return play_;
}

bool PlaySettings::getNextTrackFlag(){
    return nextTrackFlag;
}

void PlaySettings::disableNextTrackFlag(){
    nextTrackFlag = false;
}
