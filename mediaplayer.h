#pragma once


// video
#include "mediademuxer.h"
#include "video_decoder.h"
#include "format_converter.h"
#include "display.h"
#include "queue.h"
#include "timer.h"

// audio
#include "audio_decoder.h"
#include "ring_buffer.h"
#include "audio.h"

// control
#include "playsettings.h"

#include <memory>
#include <string>
#include <thread>
#include <vector>
extern "C" {
	#include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
}


class Player {
public:
	Player(const std::string &file_name);
	~Player();
	void operator()();
private:
	void demultiplex();
    void decode_video(); // video only
    void decode_audio(); // audio only
	void video();
private:
    std::unique_ptr<MediaDemuxer> demuxer_;
	std::unique_ptr<VideoDecoder> video_decoder_;
    std::unique_ptr<AudioDecoder> audio_decoder_;  //audio
	std::unique_ptr<FormatConverter> format_converter_;
	std::unique_ptr<Display> display_;
	std::unique_ptr<Timer> timer_;
    std::unique_ptr<PacketQueue> packet_queue_video_;
    std::unique_ptr<PacketQueue> packet_queue_audio_;
    std::unique_ptr<FrameQueue> frame_queue_video_;

    std::vector<std::thread> stages_;
    static const size_t queue_size_{256};

    // audio
    const size_t buffer_size{1048576};
    std::unique_ptr<RingBuffer> ring_buffer_;

    const std::string backend = "sdl";
    Audio* audio_;

    // settings
    PlaySettings* settings;

};
