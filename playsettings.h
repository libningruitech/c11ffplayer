#pragma once
#include <SDL2/SDL.h>

struct SDL_E {
    SDL_E();
    ~SDL_E();
};


class PlaySettings
{

private:
    bool quit_{false};
    bool play_{true};
    bool nextTrackFlag{false};

    SDL_E sdl_;

    SDL_Event event_;

public:
    PlaySettings();

    // Handle events
    void input();

    bool get_quit();
    bool get_play();

    bool getNextTrackFlag();
    void disableNextTrackFlag();

};

