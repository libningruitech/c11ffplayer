#include "mediaplayer.h"
#include "audio_factory.h"

#include <algorithm>
#include <chrono>
#include <iostream>
extern "C" {
	#include <libavutil/time.h>
	#include <libavutil/imgutils.h>
}

Player::Player(const std::string &file_name) :
    demuxer_{new MediaDemuxer{file_name}},
	video_decoder_{new VideoDecoder{demuxer_->video_codec_context()}},
    audio_decoder_{new AudioDecoder{demuxer_->audio_codec_context()}}, // audio
	format_converter_{new FormatConverter{video_decoder_->width(), video_decoder_->height(), video_decoder_->pixel_format(), AV_PIX_FMT_YUV420P}},
	display_{new Display{video_decoder_->width(), video_decoder_->height()}},
	timer_{new Timer},
    packet_queue_video_{new PacketQueue{queue_size_}},
    packet_queue_audio_{new PacketQueue{queue_size_}},
    frame_queue_video_{new FrameQueue{queue_size_}} ,
    ring_buffer_{new RingBufferLock{buffer_size}}, // audio
    audio_{NULL},
    settings{new PlaySettings{}}
{

    audio_= make_audio(
          backend
                , audio_decoder_.get()->sample_rate()
                , audio_decoder_.get()->format()
                , audio_decoder_.get()->channels()
                , audio_decoder_.get()->samples()
                , ring_buffer_.get());
    (*audio_)();

}

Player::~Player() {
    frame_queue_video_->quit();
    packet_queue_video_->quit();
    packet_queue_audio_->quit();

	for (auto &stage : stages_) {
		stage.join();
	}
}

void Player::operator()() {

	stages_.emplace_back(&Player::demultiplex, this);

    stages_.emplace_back(&Player::decode_video, this);
    stages_.emplace_back(&Player::decode_audio, this);
    video();


}

void Player::demultiplex() {
	try {
		for (;;) {
			// Create AVPacket
			std::unique_ptr<AVPacket, std::function<void(AVPacket*)>> packet{
				new AVPacket, [](AVPacket* p){ av_packet_unref(p); delete p; }};
			av_init_packet(packet.get());
			packet->data = nullptr;

			// Read frame into AVPacket
			if (!(*demuxer_)(*packet)) {
                packet_queue_video_->finished();
                packet_queue_audio_->finished();
				break;
			}

			// Move into queue if first video stream
			if (packet->stream_index == demuxer_->video_stream_index()) {
                if (!packet_queue_video_->push(move(packet))) {
					break;
				}
            }else if (packet->stream_index == demuxer_->audio_stream_index()) {
                if (!packet_queue_audio_->push(move(packet))) {
                    break;
                }
            }

		}
	} catch (std::exception &e) {
		std::cerr << "Demuxing error: " << e.what() << std::endl;
		exit(1);
	}
}

void Player::decode_video() {

	const AVRational microseconds = {1, 1000000};

	try {
		for (;;) {
			// Create AVFrame and AVQueue
            std::unique_ptr<AVFrame, std::function<void(AVFrame*)>> frame_video_decoded{
				av_frame_alloc(), [](AVFrame* f){ av_frame_free(&f); }};
			std::unique_ptr<AVPacket, std::function<void(AVPacket*)>> packet{
				nullptr, [](AVPacket* p){ av_packet_unref(p); delete p; }};

			// Read packet from queue
            if (!packet_queue_video_->pop(packet)) {
                frame_queue_video_->finished();
				break;
			}

            // Decode packet  -- video
			int finished_frame;
			(*video_decoder_)(
                frame_video_decoded.get(), finished_frame, packet.get());

			// If a whole frame has been decoded,
			// adjust time stamps and add to queue
			if (finished_frame) {
                frame_video_decoded->pts = av_rescale_q(
                    frame_video_decoded->pkt_dts,
					demuxer_->time_base(),
					microseconds);

				std::unique_ptr<AVFrame, std::function<void(AVFrame*)>> frame_converted{
					av_frame_alloc(),
					[](AVFrame* f){ av_free(f->data[0]); }};
				if (av_frame_copy_props(frame_converted.get(),
                    frame_video_decoded.get()) < 0) {
					throw std::runtime_error("Copying frame properties");
				}
				if (av_image_alloc(
					frame_converted->data, frame_converted->linesize,
					video_decoder_->width(), video_decoder_->height(),
					video_decoder_->pixel_format(), 1) < 0) {
					throw std::runtime_error("Allocating picture");
				}	
				(*format_converter_)(
                    frame_video_decoded.get(), frame_converted.get());

                if (!frame_queue_video_->push(move(frame_converted))) {
					break;
				}
			}

        }
	} catch (std::exception &e) {
		std::cerr << "Decoding error: " <<  e.what() << std::endl;
		exit(1);
	}

}

void Player::decode_audio(){


    try {
        for (;;) {
            // Create AVFrame and AVQueue
            std::unique_ptr<AVFrame, std::function<void(AVFrame*)>> frame_audio_decoded{
                av_frame_alloc(), [](AVFrame* f){ av_frame_free(&f); }};
            std::unique_ptr<AVPacket, std::function<void(AVPacket*)>> packet{
                nullptr, [](AVPacket* p){ av_packet_unref(p); delete p; }};

            // Read packet from queue
            if (!packet_queue_audio_->pop(packet)) {
                break;
            }

            // Decode packet  -- audio
            int finished_frame;
            (*audio_decoder_)(
                frame_audio_decoded.get(), finished_frame, packet.get());



            // put into ring
            if (finished_frame){
                //AVFrame* frame =
                size_t channels = frame_audio_decoded->channels;
                auto format = static_cast<AVSampleFormat>(frame_audio_decoded->format);
                size_t unpadded_linesize =
                    frame_audio_decoded->nb_samples * av_get_bytes_per_sample(format);
                size_t bytes = av_get_bytes_per_sample(format);
                // Pack data
                if (av_sample_fmt_is_planar(format)) {
                    for (size_t i = 0; i < unpadded_linesize; i += bytes) {
                        for (size_t channel = 0; channel < channels; ++channel) {
                            ring_buffer_.get()->push(&frame_audio_decoded->extended_data[channel][i], bytes);
                        }
                    }
                } else {
                    ring_buffer_.get()->push(
                        &frame_audio_decoded->extended_data[0][0], channels * unpadded_linesize);
                }
            }


        }
    } catch (std::exception &e) {
        std::cerr << "Decoding error: " <<  e.what() << std::endl;
        exit(1);
    }

}


void Player::video() {
	try {
		int64_t last_pts = 0;

		for (uint64_t frame_number = 0;; ++frame_number) {

            settings->input();

            // quit
            if (settings->get_quit()) {
                break;

            // pause
            } else if (settings->get_play()) {
                audio_->resume();// resume audio

                std::unique_ptr<AVFrame, std::function<void(AVFrame*)>> frame{
                    nullptr, [](AVFrame* f){ av_frame_free(&f); }};
                if (!frame_queue_video_->pop(frame)) {
                    break;
                }

                if (frame_number) {
                    const int64_t frame_delay = frame->pts - last_pts;
                    last_pts = frame->pts;
                    timer_->wait(frame_delay);

                } else {
                    last_pts = frame->pts;
                    timer_->update();
                }

                display_->refresh(
                    {frame->data[0], frame->data[1], frame->data[2]},
                    {static_cast<size_t>(frame->linesize[0]),
                     static_cast<size_t>(frame->linesize[1]),
                     static_cast<size_t>(frame->linesize[2])});

            } else {
                audio_->pause(); // pause audio

                std::chrono::milliseconds sleep(10);
                std::this_thread::sleep_for(sleep);
                timer_->update();
            }

            // change audio track
            if (settings->getNextTrackFlag()){
                demuxer_.get() ->change_audio_stream();
                settings->disableNextTrackFlag();
            }

        }
	} catch (std::exception &e) {
		std::cerr << "Display error: " <<  e.what() << std::endl;
		exit(1);
	}
}
