#include "mediademuxer.h"
#include "ffmpeg.h"

int static query_num(enum AVMediaType type, AVFormatContext *ctx)
{

    unsigned int i;

    unsigned num = 0;
    for (i = 0; (unsigned int) i < ctx->nb_streams; i++) {
        if (ctx->streams[i]->codec->codec_type == type){
            num++;
        }
    }
    return (num>0) ? num: -1;

}


MediaDemuxer::MediaDemuxer(const std::string &file_name) {
	av_register_all();
    avformat_network_init();

	ffmpeg::check(avformat_open_input(
		&format_context_, file_name.c_str(), nullptr, nullptr));
	ffmpeg::check(avformat_find_stream_info(
		format_context_, nullptr));
	video_stream_index_ = ffmpeg::check(av_find_best_stream(
		format_context_, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0));

    //audio
    audio_stream_index_ =ffmpeg::check(av_find_best_stream(
        format_context_, AVMEDIA_TYPE_AUDIO, -1, -1, nullptr, 0));

    if (audio_stream_index_< 0) {
         ffmpeg::Error("Could not find audio stream");
    }

    audio_stream_counts_ = query_num(AVMEDIA_TYPE_AUDIO, format_context_);
}

MediaDemuxer::~MediaDemuxer() {
	avformat_close_input(&format_context_);
}

AVCodecContext* MediaDemuxer::video_codec_context() {
	return format_context_->streams[video_stream_index_]->codec;
}

int MediaDemuxer::video_stream_index() const {
	return video_stream_index_;
}

AVRational MediaDemuxer::time_base() const {
	return format_context_->streams[video_stream_index_]->time_base;
}

bool MediaDemuxer::operator()(AVPacket &packet) {
	return av_read_frame(format_context_, &packet) >= 0;
}


AVCodecContext* MediaDemuxer::audio_codec_context(){
    return format_context_->streams[audio_stream_index_]->codec;
}

int MediaDemuxer::audio_stream_index() const{
    return audio_stream_index_;
}

void MediaDemuxer::change_audio_stream(){
    if( audio_stream_counts_ <= 1)
        return;

    int i = format_context_->nb_streams -1;
    while(i) {
        if (format_context_->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            if(i != audio_stream_index()){
                break;
            }
        --i;
    }

    audio_stream_index_ = i;
}

